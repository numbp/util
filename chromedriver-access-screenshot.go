package util

import (
	"fmt"
	"log"

	"github.com/sclevine/agouti"
)

func AccessAndScreenshot(url, path string) {
	driver := agouti.ChromeDriver(
		agouti.ChromeOptions("args", []string{
			"--headless", // headlessモードの指定
			"--disable-gpu",
			"--window-size=1125,2436", // ウィンドウサイズの指定
		}),
		agouti.Debug,
	)
	if err := driver.Start(); err != nil {
		log.Fatal(err)
	}
	defer driver.Stop()

	// アクセス
	page, err := driver.NewPage()
	if err != nil {
		log.Fatal(err)
	}
	page.Navigate(url)
	getSource, err := page.HTML() // htmlソースを取得
	fmt.Println(getSource)
	page.Screenshot(path) // スクリーンショット
}
